var Alexa = require('alexa-sdk');

exports.handler = function(event, context, callback) {
    var alexa = Alexa.handler(event, context);


    alexa.registerHandlers(handlers);
    alexa.execute();
};

var handlers = {
    'LaunchRequest': function () {
        this.emit('GetStainArticle');
    },

    'GetStainArticle': function () {
        var say = '<audio src=\"https://s3-eu-west-1.amazonaws.com/njbu00-polly-mp3/speech_steves_stain.mp3" />\'';
        this.emit(':tell', say );
     }
};
